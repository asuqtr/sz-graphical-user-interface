//#region ROS General

// This function connects to the rosbridge server running on the local computer on port 9090
var rbServer = new ROSLIB.Ros({
    url : 'ws://192.168.1.15:9090'
 });

 // This function is called upon the rosbridge connection event
 rbServer.on('connection', function() {
     // Write appropriate message to #feedback div when successfully connected to rosbridge
     var fbDiv = document.getElementById('feedback');
     fbDiv.innerHTML = "Connexion au serveur ROS établie";
 });

// This function is called when there is an error attempting to connect to rosbridge
rbServer.on('error', function(error) {
    // Write appropriate message to #feedback div upon error when attempting to connect to rosbridge
    var fbDiv = document.getElementById('feedback');
    fbDiv.innerHTML = "Erreur de connexion au serveur ROS";
});

// This function is called when the connection to rosbridge is closed
rbServer.on('close', function() {
    // Write appropriate message to #feedback div upon closing connection to rosbridge
    var fbDiv = document.getElementById('feedback');
    //fbDiv.innerHTML = "Connexion au serveur ROS terminée";
 });
 //#endregion


//#region Motor 1
var listener_motor1 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/motor1',
    messageType : 'std_msgs/Int16'
  });

listener_motor1.subscribe(function(message) {
    //console.log('Received message on ' + listener_motor1.name + ': ' + message.data);
    pbMotor1.value = message.data;
  });
//#endregion


//#region Motor 2
var listener_motor2 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/motor2',
    messageType : 'std_msgs/Int16'
  });

listener_motor2.subscribe(function(message) {
    //console.log('Received message on ' + listener_motor2.name + ': ' + message.data);
    pbMotor2.value = message.data;
  });
//#endregion


//#region Servo 1

// Publish
var servo1_topic = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo1',
    messageType : 'std_msgs/Int16'
});

var servo1_msg = new ROSLIB.Message({
    data : 0
});

function pubServo1() {    
    servo1_msg.data = Number(txbServo1.value);
    servo1_topic.publish(servo1_msg);
}

// Subscribe
var listener_servo1 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo1',
    messageType : 'std_msgs/Int16'
  });

  listener_servo1.subscribe(function(message) {
    //console.log('Received message on ' + listener_servo1.name + ': ' + message.data);
    txbServo1.value = message.data;
  });
//#endregion


//#region Servo 2

// Publish
var servo2_topic = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo2',
    messageType : 'std_msgs/Int16'
});

var servo2_msg = new ROSLIB.Message({
    data : 0
});

function pubServo2() {    
    servo2_msg.data = Number(txbServo2.value);
    servo2_topic.publish(servo2_msg);
}

// Subscribe
var listener_servo2 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo2',
    messageType : 'std_msgs/Int16'
  });

  listener_servo2.subscribe(function(message) {
    //console.log('Received message on ' + listener_servo2.name + ': ' + message.data);
    txbServo2.value = message.data;
  });
//#endregion


//#region Servo 3

// Publish
var servo3_topic = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo3',
    messageType : 'std_msgs/Int16'
});

var servo3_msg = new ROSLIB.Message({
    data : 0
});

function pubServo3() {    
    servo3_msg.data = 0 + Number(txbServo3.value);
    servo3_topic.publish(servo3_msg);
}

// Subscribe
var listener_servo3 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo3',
    messageType : 'std_msgs/Int16'
  });

  listener_servo3.subscribe(function(message) {
    //console.log('Received message on ' + listener_servo3.name + ': ' + message.data);
    txbServo3.value = message.data;
  });
//#endregion


//#region Servo 4

// Publish
var servo4_topic = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo4',
    messageType : 'std_msgs/Int16'
});

var servo4_msg = new ROSLIB.Message({
    data : 0
});

function pubServo4() {    
    servo4_msg.data = Number(txbServo4.value);
    servo4_topic.publish(servo4_msg);
}

// Subscribe
var listener_servo4 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo4',
    messageType : 'std_msgs/Int16'
  });

  listener_servo4.subscribe(function(message) {
    //console.log('Received message on ' + listener_servo4.name + ': ' + message.data);
    txbServo4.value = message.data;
  });
//#endregion


//#region Webcam
var listener_webcam1 = new ROSLIB.Topic({
	  ros : rbServer,
    name : '/camera_node/image_raw/compressed',
    messageType : 'sensor_msgs/CompressedImage'
  });

listener_webcam1.subscribe(function(message) {
    console.log('Received message on ' + listener_webcam1.name + ': ' + message.format);
	var cadre = document.getElementById("imagetest");
	cadre.src = "data:image/jpeg;base64," + message.data;
  });
//#endregion


//#region Joystick

var joy_topic = new ROSLIB.Topic({
  ros : rbServer,
  name : '/joy',
  messageType : 'sensor_msgs/Joy'
});

var joy_msg = new ROSLIB.Message({
axes : [0,0,0,0,0,0,0,0],
buttons :  [0,0,0,0,0,0,0,0,0,0,0]  
});

function pubJoystick(gamepadData) {   
//#region Axes
joy_msg.axes[0] = gamepadData.axes[0]; // Left stick X
joy_msg.axes[1] = gamepadData.axes[1]; // Left stick Y
joy_msg.axes[2] = gamepadData.buttons[6].value; // Left shoulder
joy_msg.axes[3] = gamepadData.axes[2]; // Right stick X
joy_msg.axes[4] = gamepadData.axes[3]; // Right stick Y
joy_msg.axes[5] = gamepadData.buttons[7].value; // Right shoulder
// DPAD Left/Right
if (gamepadData.buttons[14].pressed) {
  joy_msg.axes[6] = 1;
}
else if (gamepadData.buttons[15].pressed) {
  joy_msg.axes[6] = -1;
}
else {
  joy_msg.axes[6] = 0;
}
// DPAD Left/Right
if (gamepadData.buttons[12].pressed) {
  joy_msg.axes[7] = 1;
}
else if (gamepadData.buttons[13].pressed) {
  joy_msg.axes[7] = -1;
}
else {
  joy_msg.axes[7] = 0;
}
//#endregion

//#region Buttons

joy_msg.buttons[0] = Number(gamepadData.buttons[0].pressed); // Button A
joy_msg.buttons[1] = Number(gamepadData.buttons[1].pressed); // Button B
joy_msg.buttons[2] = Number(gamepadData.buttons[2].pressed); // Button X
joy_msg.buttons[3] = Number(gamepadData.buttons[3].pressed); // Button Y
joy_msg.buttons[4] = Number(gamepadData.buttons[4].pressed); // Button Left trigger
joy_msg.buttons[5] = Number(gamepadData.buttons[5].pressed); // Button Right trigger
joy_msg.buttons[6] = Number(gamepadData.buttons[8].pressed); // Button Back
joy_msg.buttons[7] = Number(gamepadData.buttons[9].pressed); // Button Start
joy_msg.buttons[8] = Number(gamepadData.buttons[16].pressed); // Button Xbox
joy_msg.buttons[9] = Number(gamepadData.buttons[10].pressed); // Button Click left stick
joy_msg.buttons[10] = Number(gamepadData.buttons[11].pressed); // Button Click right stick

//#endregion

joy_topic.publish(joy_msg);
}
//#endregion


//#region GamePad

var rAF = window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.requestAnimationFrame;
var rAFStop = window.mozCancelRequestAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.cancelRequestAnimationFrame;
window.addEventListener("gamepadconnected", function() {
  var gp = navigator.getGamepads()[0];
  var gamepadInfo = document.getElementById('gamepadinfo');
  gamepadInfo.innerHTML = "Contrôleur connecté avec l'index " + gp.index + ": " + gp.id + ". Il a " + gp.buttons.length + " boutons et " + gp.axes.length + " axes.";
  gameLoop();
});
window.addEventListener("gamepaddisconnected", function() {
  gamepadInfo.innerHTML = "Waiting for gamepad.";
  rAFStop(start);
});
if(!('GamepadEvent' in window)) {
  // No gamepad events available, poll instead.
  var interval = setInterval(pollGamepads, 500);
}

function pollGamepads() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
  for (var i = 0; i < gamepads.length; i++) {
    var gp = gamepads[i];
    if(gp) {
      gamepadInfo.innerHTML = "Contrôleur connecté avec l'index " + gp.index + ": " + gp.id + ". Il a " + gp.buttons.length + " boutons et " + gp.axes.length + " axes.";
      gameLoop();
      clearInterval(interval);
    }
  }
}

function buttonPressed(b) {
  if (typeof(b) == "object") {
    return b.pressed;
  }
  return b == 1.0;
}

var previousState;
function gameLoop() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
  if (!gamepads)
    return;
  var gp = gamepads[0];
  pubJoystick(gp);
  var start = rAF(gameLoop);
};

//#endregion
